var app = new Vue({
	el: '#app',
	data: {
		isModal: false,
		isModalResult: false,
		money: {
			cur: 240000,
			min: 50000,
			max: 7000000,
			summ: null,
			progress: 0
		},
		month: {
			cur: 3,
			min: 0,
			max: 12,
			summ: null,
			progress: 0
		},
		result: {
			procent: 20,
			month_pay: 50000
		}
	},
	watch: {
		money: {
			handler() {
				var res = Number(this.money.cur);
				this.money.summ = res.toLocaleString('ru');
				this.money.progress = (this.money.cur / this.money.max) * 100;

				this.getResult();
			},
			deep: true
		},
		month: {
			handler() {
				this.month.summ = this.month.cur;
				this.month.progress = (this.month.cur / this.month.max) * 100;

				this.getResult();
			},
			deep: true
		}
	},
	methods: {
		getNumber(num) {
			return Number(num).toLocaleString('ru');
		},
		getResult() {
            // Get procent
            if(this.money.cur > 500000 && this.money.cur < 1500000) {
            	this.result.procent = 18;
            }
            else if(this.money.cur > 1500000) {
            	this.result.procent = 16;
            }
            else {
            	this.result.procent = 20;
            }

            // Get month pay
            this.result.month_pay = this.month.cur > 0 ? Math.floor(this.money.cur / this.month.cur) : 0;
        },
        showResult() {
        	this.isModal = false;
        	this.isModalResult = true;
        }
    },
    mounted() {
    	this.money.summ = Number(this.money.cur).toLocaleString('ru');
    	this.month.summ = this.month.cur;
    }
});

// For working mobile menu
$('.menu-icon-toggle').on('click', function(e) {
	$('body').toggleClass('open');

	e.preventDefault();
});